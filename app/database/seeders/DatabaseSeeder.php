<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\Country;
use App\Models\Currency;
use App\Models\Rate;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Country::factory(10)->hasCities(20)->create();

        Currency::factory()->create([
            'code' => 'USD',
            'is_default' => true,
        ]);

        $currencies = Currency::factory(9)->create();

        for ($i = 0; $i < 20; $i++) {
            $date = (new \DateTime())->modify('-' . $i . ' day');

            foreach ($currencies as $c) {
                Rate::factory()->create([
                    'currency_id' => $c->id,
                    'created_at' => $date,
                ]);
            }
        }

        for ($i = 0; $i < 20; $i++) {
            User::factory()->hasAccounts(1, [
                'currency_id' => rand(1, 10),
                'value' => 0,
                'is_default' => true,
            ])->create([
                'city_id' => rand(1, 200),
            ]);
        }

        $accounts = Account::select('id', 'currency_id', 'value')->get();
        $list = [];

        foreach ($accounts as $item) {
            $list[$item->id] = $item;
        }
    }
}
