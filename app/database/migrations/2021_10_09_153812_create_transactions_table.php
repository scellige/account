<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('currency_id');
            $table->unsignedBigInteger('value');
            $table->unsignedBigInteger('value_usd');
            $table->dateTime('created_at')->useCurrent();

            $table->foreign('currency_id')->references('id')->on('currencies');
        });

        Schema::create('account_transaction', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('account_id');
            $table->bigInteger('transaction_id');
            $table->boolean('is_in');

            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('transaction_id')->references('id')->on('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropForeign(['currency_id']);
        });

        Schema::table('account_transaction', function (Blueprint $table) {
            $table->dropForeign(['account_id']);
            $table->dropForeign(['transaction_id']);
        });

        Schema::dropIfExists('transactions');
        Schema::dropIfExists('account_transaction');
    }
}
