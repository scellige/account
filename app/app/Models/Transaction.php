<?php

namespace App\Models;

use App\Models\Traits\ValueCast;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Transaction extends Model
{
    use HasFactory;
    use ValueCast;

    /**
     * Коэффициент точности
     * Все суммы хранятся в int из-за проблем в расчетах для чисел с плавающей точкой
     */
    public const PRECISION_COEFFICIENT = 10000;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'currency_id',
        'value',
        'value_usd',
        'created_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'currency_id' => 'integer',
        'created_at' => 'datetime',
    ];

    /**
     * Валюта
     *
     * @return BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    /**
     * Кошельки участвующие в транзакции
     *
     * @return BelongsToMany
     */
    public function accounts()
    {
        return $this->belongsToMany(Account::class, 'account_transaction')->withPivot('is_in');
    }

    /**
     * Модифицируем значение атрибута при сохранении
     *
     * @param int|float $value
     *
     * @return void
     */
    public function setValueUsdAttribute($value)
    {
        $this->attributes['value_usd'] = round($value * $this::PRECISION_COEFFICIENT);
    }

    /**
     * Модифицируем значение атрибута при получении
     *
     * @param int $value
     *
     * @return float
     */
    public function getValueUsdAttribute(int $value)
    {
        return $value / $this::PRECISION_COEFFICIENT;
    }
}
