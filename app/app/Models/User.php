<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class User extends Model
{
    use HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'city_id',
        'name',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'city_id' => 'integer',
    ];

    /**
     * Город
     *
     * @return BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    /**
     * Кошельки
     *
     * @return HasMany
     */
    public function accounts()
    {
        return $this->hasMany(Account::class, 'user_id');
    }

    /**
     * Кошелек по-умолчанию
     *
     * @return HasOne
     */
    public function account()
    {
        return $this->hasOne(Account::class, 'user_id')->where('is_default', true);
    }
}
