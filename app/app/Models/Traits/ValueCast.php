<?php

namespace App\Models\Traits;

use App\Models\Transaction;

/**
 * Преобразовываем значение суммы в int для хранения и точных расчетов
 */
trait ValueCast
{
    /**
     * Модифицируем значение атрибута при сохранении
     *
     * @param int|float $value
     *
     * @return void
     */
    public function setValueAttribute($value)
    {
        $this->attributes['value'] = round($value * Transaction::PRECISION_COEFFICIENT);
    }

    /**
     * Модифицируем значение атрибута при получении
     *
     * @param int $value
     *
     * @return float
     */
    public function getValueAttribute(int $value)
    {
        return $value / Transaction::PRECISION_COEFFICIENT;
    }
}
