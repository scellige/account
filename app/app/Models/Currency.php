<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Currency extends Model
{
    use HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'code',
        'is_default',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'is_default' => 'boolean',
    ];

    /**
     * Курсы валюты на разные даты
     *
     * @return HasMany
     */
    public function rates()
    {
        return $this->hasMany(Rate::class, 'currency_id')->orderBy('created_at', 'desc');
    }

    /**
     * Актуальный курс валюты
     *
     * @return HasOne
     */
    public function rate()
    {
        return $this->hasOne(Rate::class, 'currency_id')->whereDate('created_at', now());
    }

    /**
     * Переводим значение из текущей валюты в переданную
     *
     * @param Currency $currency валюта к которой нужно совершить перевод
     * @param int|float $value сумма в текущей валюте
     *
     * @return float
     */
    public function convert(Currency $currency, $value)
    {
        if ($this->id === $currency->id) {
            return $value;
        }

        // чтобы не хранить для валюты по умолчанию курс 1 на каждый день
        // проверяем наличие курса только для других валют
        if (!$currency->is_default && empty($currency->rate) || !$this->is_default && empty($this->rate)) {
            throw new Exception('currency rate does not exist');
        }

        $currentRate = $this->is_default ? 1 : $this->rate->value;
        $outerRate = $currency->is_default ? 1 : $currency->rate->value;

        return ($currentRate * $value) / $outerRate;
    }
}
