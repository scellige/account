<?php

namespace App\Http\Services;

use App\Models\City as CityModel;
use App\Models\Country as CountryModel;
use App\Models\Currency as CurrencyModel;
use App\Models\User as UserModel;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class UserService
{
    /**
     * Получаем список пользователей с постраничной навигацией
     *
     * @param int $page
     *
     * @return LengthAwarePaginator
     */
    public function list(int $page)
    {
        return UserModel::with('city', 'city.country', 'accounts', 'accounts.currency')
            ->paginate(10, ['*'], 'page', $page);
    }

    /**
     * Создаем пользователя на основе данных пользовательского ввода
     *
     * @param array $data
     * - name (string) - имя пользователя
     * - country (string) - название страны
     * - city (string) - название города
     * - currency (string) - символьный код валюты
     *
     * @return array данные созданного пользователя
     *
     * @throws ValidationException
     * @throws QueryException
     */
    public function create(array $data): array
    {
        $entity = $this->createData($data);

        DB::beginTransaction();

        try {
            /** @var UserModel */
            $user = UserModel::create([
                'name' => $data['name'],
                'city_id' => $entity['city']->id,
            ]);

            $user->account()->create([
                'currency_id' => $entity['currency']->id,
                'value' => 0,
                'is_default' => true,
            ]);
        } catch (QueryException $e) {
            DB::rollBack();

            throw $e;
        }

        DB::commit();

        $user->load('city', 'city.country', 'accounts', 'accounts.currency');

        return $user->toArray();
    }

    /**
     * Получаем данные для создания пользователя из базы
     *
     * @param array $data данные пользовательского ввода
     * - name (string) - имя пользователя
     * - country (string) - название страны
     * - city (string) - название города
     * - currency (string) - символьный код валюты
     *
     * @return array
     * - city (CityModel) - запись модели город
     * - currency (CurrencyModel) - запись модели валюта
     *
     * @throws ValidationException
     */
    protected function createData(array $data): array
    {
        $country = CountryModel::where('name', $data['country'])->first();

        if (empty($country)) {
            throw ValidationException::withMessages(['country' => 'This value does not exist']);
        }

        $city = CityModel::where('name', $data['city'])->where('country_id', $country->id)->first();

        if (empty($city)) {
            throw ValidationException::withMessages(['city' => 'This value does not exist']);
        }

        $currency = CurrencyModel::where('code', $data['currency'])->first();

        if (empty($currency)) {
            throw ValidationException::withMessages(['currency' => 'This value does not exist']);
        }

        return [
            'city' => $city,
            'currency' => $currency,
        ];
    }
}
