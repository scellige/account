<?php

namespace App\Http\Services;

use App\Http\Reports\CSV;
use App\Http\Reports\Report;
use App\Http\Reports\XML;
use App\Models\Account as AccountModel;
use App\Models\Currency as CurrencyModel;
use App\Models\Transaction as TransactionModel;
use App\Models\User as UserModel;
use DateTime;
use Illuminate\Database\Query\Builder;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ReportService
{
    /**
     * Получаем данные отчета
     *
     * @param array $data данные пользовательского ввода
     * - user_name (string) - имя пользователя
     * - date_from (?string) - дата начала периода
     * - date_to (?string) - дата окончания периода
     * - limit (int) - количество записей на странице
     * - page (int) - номер страницы
     *
     * @return array
     */
    public function get(array $data)
    {
        $user = $this->getUser($data['user_name']);

        return $this
            ->transactionsQuery($user, $data)
            ->paginate($data['limit'] ?? 1000, ['*'], 'page', $data['page'] ?? 1);
    }

    /**
     * Получаем сумму начислений и списаний по всем кошелькам пользователя за период
     *
     * @param array $data
     * - user_name (string) - имя пользователя
     * - date_from (?string) - дата начала периода
     * - date_to (?string) - дата окончания периода
     *
     * @return array
     */
    public function sum(array $data): array
    {
        $user = $this->getUser($data['user_name']);

        return $this->getSum($user->id, $data);
    }

    /**
     * Подготавливаем отчет для скачивания
     *
     * @param array $data
     * - user_name (string) - имя пользователя
     * - date_from (?string) - дата начала периода
     * - date_to (?string) - дата окончания периода
     * - format (string) - формат файла отчета
     *
     * @return array
     * - name (string) - имя файла очтета
     * - content (string) - содержимое отчета
     */
    public function download(array $data): array
    {
        $user = $this->getUser($data['user_name']);

        $transactionsList = $this->transactionsQuery($user, $data)->get()->toArray();
        $sumList = $this->getSum($user->id, $data);

        if ($data['format'] === 'csv') {
            $service = new CSV();
        } elseif ($data['format'] === 'xml') {
            $service = new XML();
        } else {
            throw ValidationException::withMessages(['format' => 'This value is not acceptable']);
        }

        return [
            'name' => $this->reportNamePrepare($data),
            'content' => $this->reportContent(
                $service,
                $this->reportCaptionPrepare($sumList),
                $this->reportBodyPrepare($user->id, $transactionsList)
            ),
        ];
    }

    /**
     * Получаем имя файла отчета на основе входных данных
     *
     * @param array $data
     * - user_name (string) - имя пользователя
     * - date_from (?string) - дата начала периода
     * - date_to (?string) - дата окончания периода
     * - format (string) - формат файла отчета
     *
     * @return string
     */
    protected function reportNamePrepare(array $data): string
    {
        $result = [
            'report',
            mb_strtolower($data['user_name']),
        ];

        if (!empty($data['date_from'])) {
            $result[] = (new DateTime($data['date_from']))->format('Y-m-d');
        }

        if (!empty($data['date_to'])) {
            $result[] = (new DateTime($data['date_to']))->format('Y-m-d');
        }

        return str_replace(' ', '-', implode('-', $result)) . '.' . $data['format'];
    }

    /**
     * Формируем содержимое отчета
     *
     * @param Report $service сервис формирования отчета
     * @param array $caption данные блока шапки
     * @param array $body данные тела отчета
     *
     * @return string
     */
    protected function reportContent(Report $service, array $caption, array $body): string
    {
        return $service->create($caption, $body);
    }

    /**
     * Формируем данные блока шапки отчета
     *
     * @param array $sumList список итоговых сумм по кошелькам клиента
     *
     * @return array
     */
    protected function reportCaptionPrepare(array $sumList): array
    {
        $result = [
            ['Кошелек', 'Тип', 'Сумма', 'Сумма USD'],
        ];

        foreach ($sumList as $row) {
            $result[] = [
                $this->acccountPrepare($row['account']),
                $this->directionPrepare($row['is_in']),
                $this->sumPrepare(floatval($row['sum'])),
                $this->sumPrepare(floatval($row['sum_usd'])),
            ];
        }

        return $result;
    }

    /**
     * Формируем данные тела отчета
     *
     * @param int $userID идентификатор пользователя для которого формируется отчет
     * @param array $transactionsList список транзакций по кошелькам пользователя
     *
     * @return array
     */
    protected function reportBodyPrepare(int $userID, array $transactionsList): array
    {
        $result = [
            ['ID', 'Дата', 'Кошелек', 'Операция', 'Подробности', 'Валюта операции', 'Сумма операции', 'Сумма USD'],
        ];

        foreach ($transactionsList as $row) {
            $accounts = $this->transactionAccountsPrepare($userID, $row['accounts']);

            $result[] = [
                $row['id'],
                (new DateTime($row['created_at']))->format('Y-m-d H:i:s'),
                $this->acccountPrepare($accounts['current']),
                $this->directionPrepare($accounts['current']['id'] === $accounts['to']['id']),
                $this->summaryPrepare($accounts['from'] ?? [], $accounts['to']),
                $row['currency']['code'],
                $this->sumPrepare(floatval($row['value'])),
                $this->sumPrepare(floatval($row['value_usd'])),
            ];
        }

        return $result;
    }

    /**
     * Получаем список кошельков участвующих в транзакции
     *
     * @param int $userID идентификатор пользователя для которого формируется отчет
     * @param array $list список транзакций
     *
     * @return array
     * - current (array) - данные кошелька пользователя для которого произошла транзакция
     * - from (array) - данные кошелька списания
     * - to (array) - данные кошелька зачисления
     */
    protected function transactionAccountsPrepare(int $userID, array $list): array
    {
        $result = [];

        foreach ($list as $item) {
            if ($item['user_id'] === $userID) {
                $result['current'] = $item;
            }

            if ($item['pivot']['is_in']) {
                $result['to'] = $item;
            } else {
                $result['from'] = $item;
            }
        }

        return $result;
    }

    /**
     * Подготавливаем сумму для вывода в отчете
     *
     * @param float $sum сумма
     *
     * @return string
     */
    protected function sumPrepare(float $sum): string
    {
        return number_format(round($sum * 100) / 100, 2, ',', '');
    }

    /**
     * Подготавливаем имя кошелька для вывода в отчете
     *
     * @param array $account данные кошелька
     *
     * @return string
     */
    protected function acccountPrepare(array $account): string
    {
        return '#' . $account['id'] . ' ' . $account['currency']['code'] . '';
    }

    /**
     * Подготавливаем имя направления перевода для текущего кошелька в транзакции
     *
     * @param bool $isIn перевод на кошелек
     *
     * @return string
     */
    protected function directionPrepare(bool $isIn): string
    {
        return $isIn ? 'Зачисление' : 'Списание';
    }

    /**
     * Подготавливаем детали транзакции если это перевод
     *
     * @param array $accountFrom данные кошелька списания
     * @param array $accountTo данные кошелька зачисления
     *
     * @return string
     */
    protected function summaryPrepare(array $accountFrom, array $accountTo): string
    {
        if (empty($accountFrom)) {
            return '';
        }

        $result = $accountFrom['user']['name'] . ' (' . $this->acccountPrepare($accountFrom) . ')';
        $result .= ' → ';
        $result .= $accountTo['user']['name'] . ' (' . $this->acccountPrepare($accountTo) . ')';

        return $result;
    }

    /**
     * Получаем запрос со списком транзакций
     *
     * @param UserModel $user данные пользователя
     * @param array $data данные пользовательского ввода
     * - user_name (string) - имя пользователя
     * - date_from (?string) - дата начала периода
     * - date_to (?string) - дата окончания периода
     *
     * @return Builder
     */
    protected function transactionsQuery(UserModel $user, array $data)
    {
        return TransactionModel::with('accounts', 'accounts.currency', 'accounts.user', 'currency')
            ->whereHas('accounts', function ($q) use ($user) {
                $q->where('user_id', $user->id);
            })
            ->where(function ($q) use ($data) {
                if (!empty($data['date_from'])) {
                    $q->whereDate('created_at', '>=', $data['date_from']);
                }

                if (!empty($data['date_to'])) {
                    $q->whereDate('created_at', '<=', $data['date_to']);
                }
            })->orderBy('created_at', 'asc');
    }

    /**
     * Получаем учетную запись пользователя
     *
     * @param string $name имя пользователя
     *
     * @return UserModel
     */
    protected function getUser(string $name): UserModel
    {
        $user = UserModel::where('name', $name)->first();

        if (empty($user)) {
            throw new NotFoundHttpException('User does not exist');
        }

        return $user;
    }

    /**
     * Получаем сумму операций по всем кошелькам пользователя за период
     *
     * @param int $userID идентификатор пользователя
     * @param array $data данные пользовательского ввода
     * - user_name (string) - имя пользователя
     * - date_from (?string) - дата начала периода
     * - date_to (?string) - дата окончания периода
     *
     * @return array
     */
    protected function getSum(int $userID, array $data): array
    {
        $list = AccountModel::where('user_id', $userID)
            ->selectRaw(
                'accounts.id, 
                account_transaction.is_in, 
                SUM(transactions.value_usd) / ' . TransactionModel::PRECISION_COEFFICIENT . ' as sum_usd'
            )
            ->join('account_transaction', 'accounts.id', '=', 'account_transaction.account_id')
            ->join('transactions', 'transactions.id', '=', 'account_transaction.transaction_id')
            ->where(function ($q) use ($data) {
                if (!empty($data['date_from'])) {
                    $q->whereDate('transactions.created_at', '>=', $data['date_from']);
                }

                if (!empty($data['date_to'])) {
                    $q->whereDate('transactions.created_at', '<=', $data['date_to']);
                }
            })
            ->groupBy('accounts.id', 'account_transaction.is_in')
            ->orderBy('id')->orderBy('is_in', 'desc')
            ->get()
            ->toArray();

        $accounts = AccountModel::where('user_id', $userID)->with('currency', 'currency.rate')->get();
        $accountMap = [];

        foreach ($accounts as $item) {
            $accountMap[$item->id] = $item;
        }

        $usdCurrency = CurrencyModel::where('code', 'USD')->first();

        foreach ($list as $k => $v) {
            $list[$k]['account'] = $accountMap[$v['id']]->toArray();
            $list[$k]['sum'] = $usdCurrency->convert($accountMap[$v['id']]->currency, $v['sum_usd']);
        }

        return $list;
    }
}
