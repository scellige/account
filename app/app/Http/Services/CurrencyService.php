<?php

namespace App\Http\Services;

use App\Models\Currency as CurrencyModel;
use App\Models\Rate as RateModel;
use DateTime;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CurrencyService
{
    /**
     * Получаем значение котировки для валюты на выбранную дату
     *
     * @param string $code символьный код валюты
     * @param DateTime $date дата получения котировки
     *
     * @return array
     */
    public function item(string $code, DateTime $date): array
    {
        $currency = CurrencyModel::where('code', mb_strtoupper($code))
            ->with(['rates' => function ($q) use ($date) {
                $q->whereDate('created_at', $date);
            }])
            ->first();

        if (empty($currency)) {
            throw new NotFoundHttpException('Currency does not exist');
        }

        if (!$currency->is_default && $currency->rates->isEmpty()) {
            throw new NotFoundHttpException('Rate does not exist');
        }

        // не храним курс основной валюты, но ответ должен быть одинаковым для всех
        if ($currency->is_default) {
            $currency->rates->push((new RateModel([
                'currency_id' => $currency->id,
                'value' => 1,
                'created_at' => $date,
            ])));
        }

        return $currency->toArray();
    }
}
