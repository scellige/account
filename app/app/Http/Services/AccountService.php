<?php

namespace App\Http\Services;

use App\Models\Account as AccountModel;
use App\Models\Currency as CurrencyModel;
use App\Models\Transaction as TransactionModel;
use App\Models\User as UserModel;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AccountService
{
    /**
     * Зачисляем средства на кошелек пользователя
     *
     * @param array $data данные пользовательского ввода
     * - user_name (string) - имя пользователя
     * - currency (string) - валюта зачисления
     * - value (number) - сумма зачисления
     * - account_id (?int) - идентификатор кошелька
     *
     * @return void
     */
    public function add(array $data)
    {
        $account = $this->getAccount($data['user_name'], $data['account_id'] ?? 0);
        $currency = $this->getCurrency($data['currency']);
        $usdCurrency = $this->getCurrency('USD');

        DB::beginTransaction();

        try {
            // создаем транзакцию
            $transaction = TransactionModel::create([
                'currency_id' => $currency->id,
                'value' => $data['value'],
                'value_usd' => $currency->convert($usdCurrency, $data['value']),
            ]);

            // создаем связь с кошельком зачисления
            $transaction->accounts()->sync([$account->id => ['is_in' => true]]);

            // зачисляем средства на кошелек в валюте кошелька
            $account->update([
                'value' => $account->value + $currency->convert($account->currency, $data['value']),
            ]);
        } catch (QueryException $e) {
            DB::rollBack();

            throw $e;
        }

        DB::commit();
    }

    /**
     * Переводим средства между кошельками
     *
     * @param array $data данные пользовательского ввода
     * - user_from (string) - имя пользователя отправителя
     * - user_to (string) - имя пользователя получателя
     * - currency (string) - валюта отправления
     * - value (number) - сумма отправления
     * - account_from (?int) - кошелек пользователя отправителя
     * - account_to (?int) - кошелек пользователя получателя
     *
     * @return void
     */
    public function transfer(array $data)
    {
        $dt = $this->transferData($data);

        DB::beginTransaction();

        try {
            // создаем транзакцию
            $transaction = TransactionModel::create([
                'currency_id' => $dt['currency']->id,
                'value' => $dt['amount']['original'],
                'value_usd' => $dt['amount']['usd'],
            ]);

            // создаем связь с кошельками
            $transaction->accounts()->sync([
                $dt['account']['from']->id => ['is_in' => false],
                $dt['account']['to']->id => ['is_in' => true],
            ]);

            // зачисляем средства на кошелек в валюте кошелька
            $dt['account']['from']->update([
                'value' => $dt['account']['from']->value - $dt['amount']['from'],
            ]);

            // зачисляем средства на кошелек в валюте кошелька
            $dt['account']['to']->update([
                'value' => $dt['account']['to']->value + $dt['amount']['to'],
            ]);
        } catch (QueryException $e) {
            DB::rollBack();

            throw $e;
        }

        DB::commit();
    }

    /**
     * Получаем запись модели валюты по символьному коду
     *
     * @param string $code
     *
     * @return CurrencyModel
     */
    protected function getCurrency(string $code): CurrencyModel
    {
        $currency = CurrencyModel::where('code', $code)->first();

        if (empty($currency)) {
            throw ValidationException::withMessages(['currency' => 'This value does not exist']);
        }

        return $currency;
    }

    /**
     * Получаем запись кошелька по имени пользователя и идентификатору
     *
     * @param string $userName имя пользователя
     * @param int $accountID идентификатор кошелька
     *
     * @return AccountModel
     */
    protected function getAccount(string $userName, int $accountID = 0): AccountModel
    {
        if (!empty($accountID)) {
            $res = AccountModel::where('id', $accountID)->with('user:id,name')->first();

            if (empty($res)) {
                throw ValidationException::withMessages(['account_id' => 'Account does not exist']);
            }

            if ($res->user->name !== $userName) {
                throw ValidationException::withMessages(['account_id' => 'Account does not belong to user']);
            }
        } else {
            $user = UserModel::where('name', $userName)->with('account')->first();

            if (empty($user)) {
                throw new NotFoundHttpException('User does not exist');
            }

            $res = $user->account;
        }

        return $res;
    }

    /**
     * Получаем данные из базы для перевода
     *
     * @param array $data данные пользовательского ввода
     * - user_from (string) - имя пользователя отправителя
     * - user_to (string) - имя пользователя получателя
     * - currency (string) - валюта отправления
     * - value (number) - сумма отправления
     * - account_from (?int) - кошелек пользователя отправителя
     * - account_to (?int) - кошелек пользователя получателя
     *
     * @return array
     * - currency (CurrencyModel) - валюта перевода
     * - account (array) - кошельки
     * - account.from (Account) - кошелек списания
     * - account.to (Account) - кошелек зачисления
     * - amount (array) - суммы в разных валютах
     * - amount.original (float) - сумма переданная (в валюте перевода)
     * - amount.from (float) - сумма в валюте кошелька списания
     * - amount.to (float) - сумма в валюте кошелька зачисления
     * - amount.usd (float) - сумма в usd
     */
    protected function transferData(array $data): array
    {
        $accountFrom = $this->getAccount($data['user_from'], $data['account_from'] ?? 0);
        $accountTo = $this->getAccount($data['user_to'], $data['account_to'] ?? 0);

        $currency = $this->getCurrency($data['currency']);

        if ($currency->id !== $accountFrom->currency_id && $currency->id !== $accountTo->currency_id) {
            throw ValidationException::withMessages(['currency' => 'Must be the currency one of accounts of transfer']);
        }

        $amountFrom = $currency->convert($accountFrom->currency, $data['value']);

        if ($amountFrom > $accountFrom->value) {
            throw ValidationException::withMessages(['value' => 'Insufficient funds on the account']);
        }

        $usdCurrency = $this->getCurrency('USD');

        return [
            'currency' => $currency,
            'account' => [
                'from' => $accountFrom,
                'to' => $accountTo,
            ],
            'amount' => [
                'original' => $data['value'],
                'from' => $amountFrom,
                'to' => $currency->convert($accountTo->currency, $data['value']),
                'usd' => $currency->convert($usdCurrency, $data['value']),
            ],
        ];
    }
}
