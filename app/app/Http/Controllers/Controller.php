<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    /**
     * Ошибка валидации
     *
     * @param ValidationException $e
     *
     * @return JsonResponse
     */
    public function validationFail(ValidationException $e): JsonResponse
    {
        return response()->json($e->errors(), $e->status);
    }

    /**
     * Ошибка выполнения запроса
     *
     * @param Throwable $e
     *
     * @return JsonResponse
     */
    public function fail(Throwable $e): JsonResponse
    {
        return response()->json($e->getMessage(), 500);
    }

    /**
     * Ошибка - сущность не найдена
     *
     * @param NotFoundHttpException $e
     *
     * @return JsonResponse
     */
    public function notFoundFail(NotFoundHttpException $e): JsonResponse
    {
        return response()->json($e->getMessage(), 404);
    }

    /**
     * Успешное выполнение запроса
     *
     * @param array $data
     *
     * @return JsonResponse
     */
    public function ok(array $data = []): JsonResponse
    {
        return response()->json($data, 200);
    }

    /**
     * Отдаем строковый файл на скачивание
     *
     * @param string $name имя файла
     * @param string $content содержимое файла
     *
     * @return StreamedResponse
     */
    public function fileString(string $name, string $content)
    {
        return response()->streamDownload(function () use ($content) {
            echo $content;
        }, $name);
    }
}
