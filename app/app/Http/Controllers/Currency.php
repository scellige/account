<?php

namespace App\Http\Controllers;

use App\Http\Requests\Currency\Item as ItemRequest;
use App\Http\Services\CurrencyService;
use DateTime;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Currency extends Controller
{
    /**
     * Сервис работы с валютами
     *
     * @var CurrencyService
     */
    protected $currensyService;

    /**
     * @param CurrencyService $currensyService сервис работы с валютами
     */
    public function __construct(CurrencyService $currensyService)
    {
        $this->currensyService = $currensyService;
    }

    /**
     * Получаем значение котировки валюты на дату
     *
     * @param CurrencyGetRequest $request данные пользовательского ввода
     * - date (string) - дата
     *
     * @return JsonResponse
     */
    public function item(ItemRequest $request, string $code)
    {
        try {
            $date = new DateTime($request->get('date'));
            $result = $this->currensyService->item($code, $date);
        } catch (ValidationException $e) {
            return $this->validationFail($e);
        } catch (NotFoundHttpException $e) {
            return $this->notFoundFail($e);
        } catch (Throwable $e) {
            return $this->fail($e);
        }

        return $this->ok($result);
    }
}
