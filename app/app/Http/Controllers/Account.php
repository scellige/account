<?php

namespace App\Http\Controllers;

use App\Http\Requests\Account\Add as AddRequest;
use App\Http\Requests\Account\Transfer as TransferRequest;
use App\Http\Services\AccountService;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Account extends Controller
{
    /**
     * Сервис работы с переводами
     *
     * @var AccountService
     */
    protected $accountService;

    /**
     * @param AccountService $accountService сервис работы с переводами
     */
    public function __construct(AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    /**
     * Зачисляем средства на кошелек пользователя
     *
     * @param AccountAddRequest $request
     * - user_name (string) - имя пользователя
     * - currency (string) - валюта зачисления
     * - value (number) - сумма зачисления
     * - account_id (?int) - идентификатор кошелька
     *
     * @return JsonResponse
     */
    public function add(AddRequest $request): JsonResponse
    {
        try {
            $this->accountService->add($request->validated());
        } catch (ValidationException $e) {
            return $this->validationFail($e);
        } catch (NotFoundHttpException $e) {
            return $this->notFoundFail($e);
        } catch (Throwable $e) {
            return $this->fail($e);
        }

        return $this->ok(['status' => true]);
    }

    /**
     * Переводим средства между кошельками
     *
     * @param AccountTransferRequest $request
     * - user_from (string) - имя пользователя отправителя
     * - user_to (string) - имя пользователя получателя
     * - currency (string) - валюта отправления
     * - value (number) - сумма отправления
     * - account_from (?int) - кошелек пользователя отправителя
     * - account_to (?int) - кошелек пользователя получателя
     *
     * @return JsonResponse
     */
    public function transfer(TransferRequest $request): JsonResponse
    {
        try {
            $this->accountService->transfer($request->validated());
        } catch (ValidationException $e) {
            return $this->validationFail($e);
        } catch (NotFoundHttpException $e) {
            return $this->notFoundFail($e);
        } catch (Throwable $e) {
            return $this->fail($e);
        }

        return $this->ok(['status' => true]);
    }
}
