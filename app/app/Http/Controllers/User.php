<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\Create as CreateUserRequest;
use App\Http\Services\UserService;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class User extends Controller
{
    /**
     * Сервис работы с пользователями
     *
     * @var UserService
     */
    protected $userService;

    /**
     * @param UserService $userService сервис работы с пользователями
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Получаем список пользователей
     *
     * @param Request $request
     *
     * @return LengthAwarePaginator
     */
    public function index(Request $request)
    {
        $page = intval($request->get('page', 1));

        return $this->userService->list($page ?? 1);
    }

    /**
     * Создаем нового пользователя
     *
     * @param CreateUserRequest $request данные пользовательского ввода
     * - name (string) - имя пользователя
     * - country (string) - название страны
     * - city (string) - название города
     * - currency (string) - символьный код валюты
     *
     * @return JsonResponse
     */
    public function create(CreateUserRequest $request)
    {
        try {
            $result = $this->userService->create($request->validated());
        } catch (ValidationException $e) {
            return $this->validationFail($e);
        } catch (QueryException $e) {
            return $this->fail($e);
        }

        return $this->ok($result);
    }
}
