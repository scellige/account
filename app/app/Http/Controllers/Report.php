<?php

namespace App\Http\Controllers;

use App\Http\Requests\Report\Get as GetRequest;
use App\Http\Requests\Report\Download as DownloadRequest;
use App\Http\Services\ReportService;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Report extends Controller
{
    /**
     * Сервис работы с отчетами
     *
     * @var ReportService
     */
    protected $reportService;

    /**
     * @param ReportService $reportService сервис работы с отчетами
     */
    public function __construct(ReportService $reportService)
    {
        $this->reportService = $reportService;
    }

    /**
     * Получаем данные отчета
     *
     * @param GetRequest $request данные пользовательского ввода
     * - user_name (string) - имя пользователя
     * - date_from (?string) - дата начала периода
     * - date_to (?string) - дата окончания периода
     * - limit (int) - количество записей на странице
     * - page (int) - номер страницы
     *
     * @return LengthAwarePaginator|JsonResponse
     */
    public function get(GetRequest $request)
    {
        try {
            $result = $this->reportService->get($request->validated());
        } catch (ValidationException $e) {
            return $this->validationFail($e);
        } catch (NotFoundHttpException $e) {
            return $this->notFoundFail($e);
        } catch (Throwable $e) {
            return $this->fail($e);
        }

        return $result;
    }

    /**
     * Получаем сумму начислений и списаний по всем кошелькам пользователя за период
     *
     * @param array GetRequest $request данные пользовательского ввода
     * - user_name (string) - имя пользователя
     * - date_from (?string) - дата начала периода
     * - date_to (?string) - дата окончания периода
     *
     * @return JsonResponse
     */
    public function sum(GetRequest $request): JsonResponse
    {
        try {
            $result = $this->reportService->sum($request->validated());
        } catch (ValidationException $e) {
            return $this->validationFail($e);
        } catch (NotFoundHttpException $e) {
            return $this->notFoundFail($e);
        } catch (Throwable $e) {
            return $this->fail($e);
        }

        return $this->ok($result);
    }

    /**
     * Получаем файл отчета
     *
     * @param array GetRequest $request данные пользовательского ввода
     * - user_name (string) - имя пользователя
     * - date_from (?string) - дата начала периода
     * - date_to (?string) - дата окончания периода
     * - format (string) - формат файла отчета
     *
     * @return StreamedResponse|JsonResponse
     */
    public function download(DownloadRequest $request)
    {
        try {
            $result = $this->reportService->download($request->validated());
        } catch (ValidationException $e) {
            return $this->validationFail($e);
        } catch (NotFoundHttpException $e) {
            return $this->notFoundFail($e);
        } catch (Throwable $e) {
            return $this->fail($e);
        }

        return $this->fileString($result['name'], $result['content']);
    }
}
