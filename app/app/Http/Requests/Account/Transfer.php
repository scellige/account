<?php

namespace App\Http\Requests\Account;

use Illuminate\Foundation\Http\FormRequest;

class Transfer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_from' => 'required|string|max:255',
            'user_to' => 'required|string|max:255',
            'currency' => 'required|string|max:10',
            'value' => 'required|numeric|gt:0',
            'account_from' => 'nullable|int|min:1',
            'account_to' => 'nullable|int|min:1',
        ];
    }
}
