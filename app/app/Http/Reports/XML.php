<?php

namespace App\Http\Reports;

use SimpleXMLElement;

class XML implements Report
{
    /**
     * Создание содержимого файла отчета
     *
     * @param array $caption блок шапки
     * @param array $body блок тела отчета
     *
     * @return string
     */
    public function create(array $caption, array $body): string
    {
        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><root></root>');

        $data = [
            'caption' => $caption,
            'body' => $body,
        ];

        foreach ($data as $groupKey => $groupData) {
            $name = [];

            foreach ($groupData as $row) {
                if (empty($name)) {
                    $name = $row;
                } else {
                    $c = $xml->addChild($groupKey);

                    foreach ($row as $k => $v) {
                        $c->addChild(str_replace(' ', '-', (string) $name[$k]), (string) $v);
                    }
                }
            }
        }

        return $xml->asXML();
    }
}
