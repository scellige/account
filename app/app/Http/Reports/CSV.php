<?php

namespace App\Http\Reports;

class CSV implements Report
{
    /**
     * Создание содержимого файла отчета
     *
     * @param array $caption блок шапки
     * @param array $body блок тела отчета
     *
     * @return string
     */
    public function create(array $caption, array $body): string
    {
        $data = [$caption, $body];

        $result = [];

        foreach ($data as $group) {
            foreach ($group as $row) {
                $result[] = implode(';', $row);
            }

            $result[] = '';
        }

        return implode("\n", $result);
    }
}
