<?php

namespace App\Http\Reports;

/**
 * Подготовка содержимого файла отчета
 */
interface Report
{
    /**
     * Создание содержимого файла отчета
     *
     * @param array $caption блок шапки
     * @param array $body блок тела отчета
     *
     * @return string
     */
    public function create(array $caption, array $body): string;
}
