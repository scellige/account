<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('App\Http\Controllers')->group(function () {
    Route::prefix('users')->group(function () {
        Route::get('', 'User@index');
        Route::post('', 'User@create');
    });

    Route::prefix('accounts')->group(function () {
        Route::post('add', 'Account@add');
        Route::post('transfer', 'Account@transfer');
    });

    Route::prefix('currencies')->group(function () {
        Route::get('{code}', 'Currency@item')->where('code', '[a-z]+');
    });

    Route::prefix('reports')->group(function () {
        Route::get('', 'Report@get');
        Route::get('sum', 'Report@sum');
        Route::get('download', 'Report@download');
    });
});
