import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            name: 'index',
            path: "/",
            component: () => import('./views/user-list.vue'),
        },
        {
            name: 'report',
            path: "/report",
            component: () => import('./views/report.vue'),
        }
    ]
});

export default router;
