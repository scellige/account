<?php

$finder = PhpCsFixer\Finder::create()->in(__DIR__);

$config = new PhpCsFixer\Config();

return $config
    ->setRules([
        '@Symfony' => true,
        'linebreak_after_opening_tag' => true,
        'yoda_style' => false,
        'phpdoc_summary' => false,
        'array_syntax' => ['syntax' => 'short'],
        'increment_style' => ['style' => 'post'],
        'concat_space' => ['spacing' => 'one'],
        'phpdoc_no_empty_return' => false,
        'phpdoc_align' => false,
        'phpdoc_add_missing_param_annotation' => true,
        'phpdoc_separation' => true,
        'ordered_class_elements' => true,
        'ordered_imports' => true,
        'no_superfluous_phpdoc_tags' => false,
    ])
    ->setFinder($finder);
